
typedef struct {
  unsigned phoneid;
  int valid;
  double price;
  char *model ;
  char *customer_name;
  char *customer_address;
  
} phone;
 
phone *make_phone(unsigned phoneid);
void free_phone(phone *connector);
void make_phone_valid(phone *connector);
void set_phone_model(phone *connector, char *model);
int is_phone_valid(phone *connector);
  
int serialize_phone(char *buffer, phone *connector);
int deserialize_phone(char *buffer, phone *connector);
void print_phone(phone *connector);
phone *alloc_blank_phone();
void set_customer_name(phone *connector);
void  get_customer_address(phone *connector);
void phone_price(phone *connector, double price);

