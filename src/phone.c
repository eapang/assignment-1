#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "config.h"// auto generated 
#include "phone.h"

phone *make_phone(unsigned phoneid)

{
  
  phone *connector; // this line declare a pointer named phone that point to phone
  
  if ((connector = (phone *)malloc(sizeof(phone))) == NULL)// check and return the adress 0 if there is not enough memory available
    {
      printf("Failed to allocate phone structure!\n"); // 
      exit(EXIT_FAILURE);
  
    }
  
  connector->valid = 0; // initialise valid to zero
  connector->phoneid = phoneid;// initialise &phoneid with phoneid
  connector->model = NULL;// intialise model with zero
  connector->customer_name = NULL; 
  connector->customer_address = NULL;
  connector->price = 0; 
  
  return connector; // return the value of the pointer stud
}

void free_phone(phone *connector) //  this line initialize the pointer stud, making the block available again.
{
  free(connector->customer_name); 
  free(connector->customer_address); 
  free(connector->model);
  free(connector);
}

void set_customer_name(phone *connector)// this function get the phone model and copy it into &model
{ 
  char customer_name[1000];
  printf("Enter  customer name:");
  connector->customer_name = strdup(gets(customer_name));
}

void get_customer_address(phone *connector)// get the customer address
{
  char address[1000];
  printf("Enter the custumer adress:");
  connector->customer_address = strdup(gets(address));
}

void make_phone_valid(phone *connector)// this line set pointer connector pointing to valid to 1
{
  connector->valid = 1;
}

void set_phone_model(phone *connector, char *model)// this function get the phone model and copy it into &model
{  
  connector->model = strdup(model);

}

void phone_price( phone *connector, double  price )// get the phone price
{
 
  connector->price = price;
}

int is_phone_valid(phone *connector) // this function check is pointer connector pointing to valid is set to 1
{
  return connector->valid;
}

int serialize_phone(char *buffer, phone *connector)// this function copy the phone info to array buffer and  returns the phone info size
{
  size_t offset = 0;

  memcpy(buffer, &connector->phoneid, sizeof(connector->phoneid)); 
  offset = sizeof(connector->phoneid);

  memcpy(buffer+offset, &connector->price, sizeof(connector->price));
  offset = offset + sizeof(connector->price);


  memcpy(buffer+offset, &connector->valid, sizeof(connector->valid));
  offset = offset + sizeof(connector->valid);

 
  memcpy(buffer+offset, connector->customer_name, strlen(connector->customer_name)+1);
  offset = offset + strlen(connector->customer_name)+1;

  memcpy(buffer+offset, connector->model, strlen(connector->model)+1);  
  offset = offset + strlen(connector->model)+1; 

  memcpy(buffer+offset, connector->customer_address, strlen(connector->customer_address)+1);
  offset = offset + strlen(connector->customer_address)+1; 

  return offset;     // this line returns the total bytes used
}

int deserialize_phone(char *buffer, phone *connector)    //this function copy the phone info from the buffer array to connector->phoneid
{
  size_t offset = 0; // initializing offset to 0

  memcpy(&connector->phoneid, buffer, sizeof(connector->phoneid));
  offset = sizeof(connector->phoneid);

  memcpy(&connector->price, buffer+offset, sizeof(connector->price));
  offset = offset + sizeof(connector->price);

  memcpy(&connector->valid, buffer+offset, sizeof(connector->valid));
  offset = offset + sizeof(connector->valid);

  memcpy(connector->customer_name, buffer+offset, strlen(buffer+offset)+1);
  offset = offset + strlen(buffer+offset)+1;

  memcpy(connector->model, buffer+offset, strlen(buffer+offset)+1);
  offset = offset + strlen(buffer+offset)+1;
 
  memcpy(connector->customer_address, buffer+offset, strlen(buffer+offset)+1);
  offset = offset + strlen(buffer+offset)+1;
 
  return offset;
}

void print_phone(phone *connector)  // this function print the phone id,customer name and adress, phone model and phone price. .
{
  printf("********************************************\n");
  printf("**  customer name is:      %s\n", connector->customer_name);  
  printf("**  customer address is:   %s\n", connector->customer_address);
  printf("**  phone ID:              %d\n", connector->phoneid);
  printf("**  phone model:           %s\n", connector->model); 
  printf("**  phone price:           £ %g\n", connector->price); 
  printf("********************************************\n");
}

phone *alloc_blank_phone()  // this function initialize the valid and phone id then check if phone model have been allocated
{

  phone *connector;

  if ((connector = (phone *)malloc(sizeof(phone))) == NULL) {
    fprintf(stderr, "Failed to allocate Student structure!\n");
    exit(EXIT_FAILURE);
  }
  connector->valid = 0;
  connector->phoneid = 0;
  connector->price = 0;

  if ((connector->model = malloc(MAX_MODEL)) == NULL) {
    fprintf(stderr, "Failed to allocate name!\n");
    exit(EXIT_FAILURE);
  }

  if ((connector->customer_name = malloc(MAX_CUSTOMER)) == NULL) {
    fprintf(stderr, "Failed to allocate name!\n");
    exit(EXIT_FAILURE);
  }
  if ((connector->customer_address = malloc(MAX_ADDRESS)) == NULL) {
    fprintf(stderr, "Failed to allocate name!\n");
    exit(EXIT_FAILURE);
  }




  return connector;
}

