
#include <sys/socket.h>
#include <sys/types.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <string.h>


#include <stdio.h>    // this line includ the standard I/O library into the program
#include <stdlib.h>

#include "phone.h"
#include "config.h"   // auto generated


int main()
{
  phone *con1;      // declare a pointer named con1 that point to phone
  

  int       sock;    // create a space called sock that can hold one integer value
                                                 
  struct    sockaddr_in servaddr;  
  char      buffer[MAX_BUFFER];                    
  ssize_t bytes_encoded, bytes_sent;

  if ( (sock = socket(AF_INET, SOCK_STREAM, 0)) < 0 ) {
    fprintf(stderr, "Error creating socket.\n");
    exit(EXIT_FAILURE);
  }
  memset(&servaddr, 0, sizeof(servaddr));
  servaddr.sin_family      = AF_INET;
  servaddr.sin_addr.s_addr = inet_addr(HOST_IP);
  servaddr.sin_port        = htons(HOST_PORT);

  if (connect(sock, (struct sockaddr *) &servaddr, sizeof(servaddr)) < 0) {
    fprintf(stderr, "Error calling connect()\n");
    exit(EXIT_FAILURE);
  }


  
  con1 = make_phone(1001);// assign phone id which is 1001 to con1
  set_customer_name(con1); // get customer name
  get_customer_address(con1);// get customer address
  make_phone_valid(con1);// this line valided the phone
  set_phone_model(con1, "nokia handset");// this line set the phone model
  phone_price(con1, 2.90);
 

  bytes_encoded = serialize_phone(buffer, con1);//count bytes encoded 
  printf("bytes_encoded = %d\n", (int)bytes_encoded);//print byte encoded

  bytes_sent = send(sock, buffer, bytes_encoded, 0);// assign byte_encoded to bytes sent

  if (bytes_sent != bytes_encoded) // check if bytes_sent is different from  byte_encoded
    {
      fprintf(stderr, "Error calling send()\n");// if different print Error calling Send
      exit(EXIT_FAILURE);// exit function
    }

  if ( close(sock) < 0 )
    {
      fprintf(stderr, "Error calling close()\n");
      exit(EXIT_FAILURE);
    }
  
  free_phone(con1) ; // return the pointer con1 to its original
                   // uninitialized state and make the block available again

  return EXIT_SUCCESS;//this line cause the main function to return EXIT_SUCCESS
  
}
